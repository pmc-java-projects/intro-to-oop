package strikes;

public class WaterStrike {
    public static void callTomahawk() {
        System.out.println("Deploying Tomahawk");
    }

    public static void callSeaToAirStrike() {
        System.out.println("Deploying Sea-To-Air Strike");
    }
}
