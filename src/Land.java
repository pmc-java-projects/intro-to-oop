public abstract class Land extends Soldier {
    Land() {
        super("A Generic Land Soldier");
    }

    public void showSecondaryWeapon() {
        System.out.println("My secondary weapon is: Handgun");
    }
}
