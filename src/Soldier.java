public abstract class Soldier {
    String name;

    Soldier() {
        this("A Generic Soldier");
    }

    Soldier(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void getName() {
        System.out.println("My name is: " + name);
    }

    public abstract void showSecondaryWeapon();
}
