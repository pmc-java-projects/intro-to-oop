public abstract class Sea extends Soldier {
    Sea() {
        super("A Generic Sea Soldier");
    }

    public void showSecondaryWeapon() {
        System.out.println("My secondary weapon is: Revolver");
    }
}
