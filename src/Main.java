import strikes.LandStrike;

public class Main {
    public static void main(String args[]) {
        Marine private1 = new Marine();

        private1.setName("Private McMillan");
        private1.getName();
        private1.showPrimaryWeapon();
        private1.showSecondaryWeapon();

        Seaman private2 = new Seaman();

        private2.getName();
        private2.showSecondaryWeapon();

        Paratrooper private3 = new Paratrooper();

        private3.getName();
        private3.showPrimaryWeapon();
        private3.showSecondaryWeapon();

        Airman private4 = new Airman();

        private4.getName();
        private4.showSecondaryWeapon();

        Infantryman private5 = new Infantryman();

        private5.getName();
        private5.showPrimaryWeapon();
        private5.showSecondaryWeapon();

        Commando private6 = new Commando();

        private6.getName();
        private6.showPrimaryWeapon();
        private6.showSecondaryWeapon();

        Engineer private7 = new Engineer();

        private7.getName();
        private7.showSecondaryWeapon();

        LandStrike.callNuke();
    }
}
