public class GalacticSoldier extends Commando {
    public void showPrimaryWeapon() {
        System.out.println("My primary weapon is: M41A Pulse Rifle");
    }

    public void showSecondaryWeapon() {
        System.out.println("My secondary weapon is: Lightsaber");
    }
}
