public abstract class Air extends Soldier {
    Air() {
        super("A Generic Air Soldier");
    }

    public void showSecondaryWeapon() {
        System.out.println("My secondary weapon is: Revolver");
    }
}
